#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "qsshclient.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void on_connectionRefreshPB_clicked();
    void on_connectPB_clicked();
    void on_disconnectPB_clicked();

    void on_downloadRefreshPB_clicked();
    void on_startDownloadPB_clicked();
    void on_stopDownloadPB_clicked();

private:
    void saveSettings();
    void readSettings();
    Ui::MainWindow *ui;    

    QSshClient m_client;
};

#endif // MAINWINDOW_H
