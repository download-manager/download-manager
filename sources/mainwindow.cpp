#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QSettings>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    readSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectionRefreshPB_clicked()
{
    if (m_client.isConnected())
        ui->connectionStatusL->setText(trUtf8("connected"));
    else
        ui->connectionStatusL->setText(trUtf8("disconnected"));
}

void MainWindow::on_connectPB_clicked()
{
    if (m_client.isConnected())
        QMessageBox::information(this, trUtf8("Connection"), trUtf8("Already connected"));
    else if (ui->userLE->text().isEmpty())
        QMessageBox::warning(this, trUtf8("Connection"), trUtf8("No user specified"));
    else if (ui->hostLE->text().isEmpty())
        QMessageBox::warning(this, trUtf8("Connection"), trUtf8("No host specified"));
    else {
        bool result = m_client.connectToHost(ui->userLE->text(), ui->hostLE->text(), ui->portSB->value());

        if (result) {
            ui->tracePTE->appendPlainText(trUtf8("Connection succeed : %1@%2:%3").arg(ui->userLE->text()).arg(ui->hostLE->text()).arg(ui->portSB->value()));
            saveSettings();
        } else
            ui->tracePTE->appendPlainText(trUtf8("Connection failed : %1@%2:%3\n   %4").arg(ui->userLE->text()).arg(ui->hostLE->text()).arg(ui->portSB->value()).arg(m_client.getLastError()));
    }

    on_connectionRefreshPB_clicked();
    on_downloadRefreshPB_clicked();
}

void MainWindow::on_disconnectPB_clicked()
{
    if (!m_client.isConnected())
        QMessageBox::information(this, trUtf8("Disconnect"), trUtf8("Already disconnected"));
    else
        m_client.disconnect();

    ui->tracePTE->appendPlainText(trUtf8("Disconnected"));
    on_connectionRefreshPB_clicked();
    on_downloadRefreshPB_clicked();
}

void MainWindow::on_downloadRefreshPB_clicked()
{
    QString result;

    if (!m_client.isConnected())
        ui->downloadStatusL->setText(trUtf8("unknow"));
    else {
        m_client.remoteExecute("ps aux | grep redkite", result);
        if (result.contains("/curl"))
            ui->downloadStatusL->setText(trUtf8("downloading"));
        else
            ui->downloadStatusL->setText(trUtf8("stopped"));
    }
}

void MainWindow::on_startDownloadPB_clicked()
{

}

void MainWindow::on_stopDownloadPB_clicked()
{
    QString processes;
    QRegExp re("redkite\\s+(\\d+).*(curl|plowdown).*");

    if (!m_client.isConnected())
        QMessageBox::warning(this, trUtf8("Stop"), trUtf8("Not connected"));
    else {
        m_client.remoteExecute("ps aux | grep redkite", processes);
        if (!processes.contains("/curl"))
            QMessageBox::information(this, trUtf8("Stop"), trUtf8("Already not downloading"));
        else {
            QString killingCommand("kill ");
            QStringList listOfProcess = processes.split('\n');
            foreach(QString s, listOfProcess)
                if (re.exactMatch(s))
                    killingCommand.append(QString("%1 ").arg(re.cap(1)));

            killingCommand = killingCommand.simplified();
            ui->tracePTE->appendPlainText(trUtf8("Stopping download : '%1'").arg(killingCommand));
            m_client.remoteExecute(killingCommand, processes);
        }
    }
    on_downloadRefreshPB_clicked();
}

void MainWindow::saveSettings()
{
    QSettings settings;

    settings.setValue("username", ui->userLE->text());
    settings.setValue("host", ui->hostLE->text());
    settings.setValue("port", ui->portSB->value());
}

void MainWindow::readSettings()
{
    QSettings settings;
    ui->userLE->setText(settings.value("username").toString());
    ui->hostLE->setText(settings.value("host").toString());
    ui->portSB->setValue(settings.value("port").toInt());
}

