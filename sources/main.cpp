#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("RedKite");
    a.setOrganizationDomain("redkite.be");
    a.setApplicationName("Download Manager");

    MainWindow w;
    w.show();

    return a.exec();
}
