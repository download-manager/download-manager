#ifndef QSSHCLIENT_H
#define QSSHCLIENT_H

#include <QObject>

#include "../libs/libssh.h"

class QSshClient : public QObject
{
    Q_OBJECT

public:
    explicit QSshClient(QObject *parent = 0);
    ~QSshClient();

    bool isConnected() const;
    bool connectToHost(const QString & username, const QString & hostname, int port=22);
    void disconnect();

    int remoteExecute(const QString & command, QString & result);
    int getRemoteProcesses(QString & data);

    QString getLastError() const;

signals:
    void connected();
    void disconnected();
    void readyRead();

public slots:

private:
    ssh_session m_sshSession;
    QString m_lastError;
};

#endif // QSSHCLIENT_H
