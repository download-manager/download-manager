#include "qsshclient.h"

#include <QtDebug>
#include <QInputDialog>

QSshClient::QSshClient(QObject *parent)
    :QObject(parent)
    ,m_sshSession(ssh_new())
{
    if (m_sshSession == NULL)
        m_lastError = "Error allocating new ssh_session";
}

QSshClient::~QSshClient()
{
    ssh_disconnect(m_sshSession);
    ssh_free(m_sshSession);
}

bool QSshClient::isConnected() const
{
    return ssh_is_connected(m_sshSession) == 1;
}

bool QSshClient::connectToHost(const QString &username, const QString &hostname, int port)
{
    if (m_sshSession == NULL)
        return false;

    // PROBLEM : When simply disconnecting and then reconnecting, it hangs on ssh_userauth_password(...)
    //           so I rebuild the sshSession
    ssh_free(m_sshSession);
    m_sshSession = ssh_new();


    // Check if the session is not already connected
    if (isConnected())
    {
        m_lastError =  QString("Error connecting to %1 : Already connected").arg(hostname);
        return false;
    }

    // Set options
    ssh_options_set(m_sshSession, SSH_OPTIONS_USER, username.toLatin1().constData());
    ssh_options_set(m_sshSession, SSH_OPTIONS_HOST, hostname.toLatin1().constData());
    ssh_options_set(m_sshSession, SSH_OPTIONS_PORT, &port);

    // Connect to server
    if (ssh_connect(m_sshSession) != SSH_OK)
    {
        m_lastError = QString("Error connecting to \"%1\" : %2").arg(hostname).arg(ssh_get_error(m_sshSession));
        return false;
    }

    // Authenticate ourselves
    bool ok;
    const char *password = QInputDialog::getText(NULL, trUtf8("libssh"), trUtf8("Enter your password :"), QLineEdit::Password, QString(), &ok).toLatin1().constData();
    if (!ok)
    {
        m_lastError = QString("Error authenticating with password: Operation canceled by the user");
        ssh_disconnect(m_sshSession);
        return false;
    }

    if (ssh_userauth_password(m_sshSession, NULL, password) != SSH_AUTH_SUCCESS)
    {
        m_lastError = QString("Error authenticating with password: %1").arg(ssh_get_error(m_sshSession));
        ssh_disconnect(m_sshSession);
        return false;
    }

    return true;
}

void QSshClient::disconnect()
{
    ssh_disconnect(m_sshSession);
}

int QSshClient::remoteExecute(const QString &command, QString &result)
{
    ssh_channel channel;
    int rc;
    char buffer[256];
    int nbytes;

    channel = ssh_channel_new(m_sshSession);
    if (channel == NULL)
      return SSH_ERROR;

    rc = ssh_channel_open_session(channel);
    if (rc != SSH_OK)
    {
      ssh_channel_free(channel);
      return rc;
    }

    rc = ssh_channel_request_exec(channel, command.toLatin1().constData());
    if (rc != SSH_OK)
    {
      ssh_channel_close(channel);
      ssh_channel_free(channel);
      return rc;
    }

    nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    while (nbytes > 0)
    {
      result.append(buffer);
      nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    }

    if (nbytes < 0)
    {
      ssh_channel_close(channel);
      ssh_channel_free(channel);
      return SSH_ERROR;
    }

    ssh_channel_send_eof(channel);
    ssh_channel_close(channel);
    ssh_channel_free(channel);

    return SSH_OK;
}

/*
int QSshClient::getRemoteProcesses(QString &data)
{
  ssh_channel channel;
  int rc;
  char buffer[256];
  unsigned int nbytes;

  channel = ssh_channel_new(m_sshSession);
  if (channel == NULL)
    return SSH_ERROR;

  rc = ssh_channel_open_session(channel);
  if (rc != SSH_OK)
  {
    ssh_channel_free(channel);
    return rc;
  }

  rc = ssh_channel_request_exec(channel, "ps aux");
  if (rc != SSH_OK)
  {
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    return rc;
  }

  nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
  while (nbytes > 0)
  {
    data.append(buffer);
    nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
  }

  if (nbytes < 0)
  {
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    return SSH_ERROR;
  }

  ssh_channel_send_eof(channel);
  ssh_channel_close(channel);
  ssh_channel_free(channel);

  return SSH_OK;
}
*/
QString QSshClient::getLastError() const
{
    return m_lastError;
}

