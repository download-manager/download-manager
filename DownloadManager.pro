#-------------------------------------------------
#
# Project created by QtCreator 2011-02-13T14:49:52
#
#-------------------------------------------------

QT       += core gui
LIBS     += -lssh

TARGET = DownloadManager
TEMPLATE = app

SOURCES += \
    sources/main.cpp \
    sources/qsshclient.cpp \
    sources/mainwindow.cpp

HEADERS += \
    sources/qsshclient.h \
    sources/mainwindow.h

FORMS += \
    uis/mainwindow.ui

RESOURCES += \
    icons.qrc
